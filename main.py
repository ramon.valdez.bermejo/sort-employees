# Final Project Course 4 Python automation.

import csv
import operator
import datetime

def get_start_date():
    """Interactively get the start date to query for."""

    print()
    print('Getting the first start date to query for.')
    print()
    print('The date must be greater than Jan 1st, 2018')
    year = int(input('Enter a value for the year: '))
    month = int(input('Enter a value for the month: '))
    day = int(input('Enter a value for the day: '))
    print()

    return datetime.datetime(year, month, day)


def get_employess(file):
    #Get csv rows ordered by date in ascending order
    with open(file, 'r') as csvFile:
        reader = csv.reader(csvFile)
        sortedlist = sorted(reader, key=operator.itemgetter(2))

    return sortedlist

def get_greater_date(start_date, sorted_list):
    #Filter sorted_list getting rid of the dates before start_date
    filtered_list = list(filter(lambda x: datetime.datetime.strptime(x[2], '%Y-%m-%d') >= start_date, sorted_list))
    employees = {}
    #Groups in employees dictionary
    #Date as key
    #List of employee names as values
    for row in filtered_list:
        #Convert string to date
        ingress_date = datetime.datetime.strptime(row[2], '%Y-%m-%d')
        # Add date to dict if not exists
        if ingress_date not in employees:
            employees[ingress_date] = []
        # Add Name attribute as a new list
        employees[ingress_date].append(row[0])
    return employees

def list_newer(start_date):
    #data = get_file_lines(FILE_URL)
    #reader = csv.reader(data[1:])
    #for row in reader:
    #    print(row)

    #for eachline in sortedlist:
     #  print(eachline)
    # while start_date < datetime.datetime.today():
    #     start_date, employees = get_same_or_newer(start_date, sorted_list)
    #     print("Started on {}: {}".format(start_date.strftime("%b %d, %Y"), employees))
    #
    #     # Now move the date to the next one
    #     start_date = start_date + datetime.timedelta(days=1)
    employees = get_greater_date(start_date, get_employess("employees.csv"))
    for key in employees:
       print("Started on {}: {}".format(key.strftime("%b %d, %Y"), employees[key]))

def main():
    start_date = get_start_date()
    list_newer(start_date)
    #list_newer(start_date, get_employess("employees.csv"))
    #get_employess("employees.csv")
    #get_greater_date(start_date, get_employess("employees.csv"))

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
